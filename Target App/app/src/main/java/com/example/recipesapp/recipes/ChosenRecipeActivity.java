package com.example.recipesapp.recipes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.recipesapp.R;

public class ChosenRecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chosen_recipe);
    }
}